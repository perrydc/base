*Base is a simple flask wrapper for deploying a microservice to a production server.  The repository is public and can be downloaded at [gitlab.com/perrydc/base](https://gitlab.com/perrydc/base).*

**Base is also a headless CRM**, allowing simple storage and retrieval of data for any authenticated user. Adding data is as simple as creating an HTML form element with the **userFormElement** class and a unique id that defines the key (no javascript needed). Retrieve data outside a form element with a single command: **window.userData[key]**

### Installation

One of the reasons I think Flask is not such a popular option is that the initial deployment is a pain in the tuchus on a traditional virtual host (though once you've set it up it only takes seconds to deploy changes).  

Although it is much easier to deploy this app with a service like [Heroku](https://stackabuse.com/deploying-a-flask-application-to-heroku/), I've documented the more difficult task of deploying on a traditional cPanel host below.  

#### Run locally:
1. Pull it from git and navigate to the project root:
```
git clone git@gitlab.com:perrydc/base.git your_project
cd your_project
```
2. Type r in terminal (assuming you have already installed the **useful aliases** below)
3. Navigate to http://127.0.0.1:5000/ in your browser
4. (optional) Add the following lines to your **.bash_profile** to streamline testing and deployment.
```
alias push='git add .; git commit -m "update"; git push'
alias r='pkill -f flask; python run.py;'
```
5. BEFORE MAKING ANY CHANGES, follow steps below to make it your own.

#### Make it your own:
Base is intended to be a foundation for projects that maximize Python's full 
stack and minimize the nonsense.  The fastest way to spin up a new project:

1. After cloning (see step 1 above) ...
2. In terminal, decouple your clone from the **base** repo:
```
cd your_project
git remote remove origin
```
3. Select **Create blank project** on the [gitlab New project page](https://gitlab.com/projects/new) or in github.
4. Give your project a name and select **Create project**
5. If the online git account you're using is not the same as the account in your local config, first click on **Members** in the lower left corner of the screen and add your local git account as a maintainer.
6. Untick the box that says **Initialize repository with a README**
7. Click **Clone** and copy the {clone_origin} for the next step (I prefer *Clone with SSH* method)
7. Send over your local code (if you've set up 2FA and SSA keys, use the :
```
cd existing_repo
git remote add origin {clone_origin}
git branch -M main
git push -uf origin main
```
7. (optional) replace favicon.ico in static/img with your own 64x64 and delete all other images.
8. (optional) Delete all the documentation in this file and replace it with your own (you can always refer back to it on github)
9. (optional) Go to templates/components/footer.html and put your org name where it says 'You'

#### Deploy:
If you are running this in its own **subdomain**, set that up first 
in **cPanel -> Domains** and set up the https cert in 
**cPanel -> Let's Encrypt SSL** (note that if you are using a caching 
layer like Cloudflare, you'll need to first add the subdomain to the 
DNS Settings, with the default toggle on proxy.  It's also a good idea 
to be in development mode when you issue the cert). 

(Enter that https domain (not the folder) as the App Domain/URI in step 5 below)

1. Clone the repository onto your production server
```
git clone git@gitlab.com:yourGitID/your_project.git
```
2. Create a backup of your run.py file since it will get overwritten by cPanel in the next step
```
cd your_project
```
```
mv run.py runBU.py
```
3. Open 'Setup Python App' from cPanel  
![cPanel setup](https://base.sourcewolf.com/static/img/cPanel_new_app.png)  
4. Select Python version > 3.0
5. Select App directory (example: python/your_project)
6. Select App Domain/URI (example: your_project.yoursite.com)
7. Set startup file as **run.py** and entry point as **application**
8. optional: set log file location, like **logs/your_project**
9. optional: set environment variables (secrets you don't want in your repo)
10. Click **CREATE** in the upper right corner
11. You should now see a link to "enter to virtual environment." Copy that command to your clipboard and enter it as an alias in your **server's .bash_profile**.  It'll look something like **alias apps='source ~/virtualenv/python/your_project/3.7/bin/activate && cd ~/python/your_project'**.  While you're at it, these two (totally optional) lines will also come in handy, if 
they're not already on your server:
```
alias pull='git pull; touch tmp/restart.txt'
```
```
alias c='env EDITOR=nano crontab -e'
```
12. Once you save, remember to source your bashprofile so these aliases will be available immediately:
```
source ~/.bashrc
```
13. When you hit CREATE a few steps back, you overwrote your run.py file, so you'll see 
**It works!** if you navigate to the domain (which is the overwritten cPanel code).  
14. To reinstate your code, just copy over that backup file you created a few steps back and rebase:
```
mv runBU.py run.py
```
```
git config pull.ff only
```
15. Install dependencies.  The best way to do this is to activate the virtual host in shell using the handy **your_project** alias you just created.  From there, 
start installing dependencies, occassionally checking to see what's missing 
by typing **python run.py**.  At the very least, you'll probably need:
```
pip install flask flask_cors python-dateutil flask-misaka requests peewee python-dotenv
```
18. After you've run **python run.py** a few times from your application folder, you'll stop seeing dependency errors and you'll see an error like **Address already in use** when you type **python run.py**.  You're almost there!  Use that **pull** alias you set up earlier to **restart the app**.  It should work now when you navigate to the address, and if it doesn't you'll see what's wrong with your code if you followed the optional step above.

16. (recommended) For error handling, navigate to the domain root folder (something like ~/apps.yourdomain.com) and add the following near the top (on the second line) of your .htaccess file (this isn't in the repo because the public site you set up in cPanel sits outside the repo):
```
PassengerFriendlyErrorPages on
```
17. (recommended) For security, you should also force https on your domain (assuming you have followed the above instructions to create an ssl cert for your domain) by adding the following lines to the bottom of your .htaccess file:
```
RewriteEngine On 
RewriteCond %{HTTPS} off 
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
```

*When you pull new code from the repo, don't forget to use that **pull** alias in this folder instead of git pull.  That will ensure you restart the app and see your changes whenever you update from the repo.*

### Learning Flask
There are a few things you should study to understand the simplicity of Flask development.  To see how you can query function methods from a template file, for instance, see the **strftime** function definition in run.py and its example usage to update the year in the footer template.  Try adding a random key value pair to the **example_json_endpoint** to see how simple it is to build generic handlers for functions.  Finally, take a moment to understand the file structure, from how the **base.html template** combines a simple page template and reused elements such as header, nav and footer, to the way that the **/static/** folder holds assets such as the favicon and the image pulled into markdown below.

#### Handling Secrets (environment variables)
If you have secrets (like the keys you'll set up to run our authentication service), the app will need access to those keys.  There are a number of ways to do this, but the simplist is to save them in a .env file and ignore that file in .gitignore.  By doing that, you'll ensure that no one sees your cheese in a public github repo.

**GAH! Where's my .env file?** It's right there in the root of this application.  You probably don't see it because it's hidden by whatever file system you're using (security through obscurity).  I use jupyter for development, so the way I edit my .env file on local is by openning another file and then changing the name of the file in the browser window to **.env**.  You'll need to edit this in production also, because your .env file isn't going to port over from your repo.  I'll let you emacs and vim enthusiasts argue amongst yourselves.  I use nano.  So just open the application folder in your production environment and type:

```
nano .env
```

Finally, keep in mind that, although Flask automagically loads environment variables as part of the package (which can be accessed via **os.environ.get('SECRET')**, your other python processes running outside Flask do not.  When you add your own, make sure you include these lines:

```
from dotenv import load_dotenv
load_dotenv()
```

### Basic Javascript API Calls
As stated at the top, you don't need javascript to store or retrieve data inside form elements, and you can retrieve data outside form elements by calling local storage like: **window.userData[key]**  More complex use-cases will require a little javascript, but not much, thanks to helper functions.  The most common use case will be an asynchronous function (which you should put in your footer) that will be a slightly modified version of the last function in the authFoot.js portion of this package.  As an example, imagine you have an HTML element like:

```html
Hello <div id='userName'></div>!
```

To populate that element with the user's first name, pop this function somewhere after that element:

```js
async function displayUserName() {
    if (typeof window.userData === "undefined") {
        window.userData = await postJSON(bsPath + profileEndpoint, fetchToken());
    }
    document.getElementById("userName").innerHTML = window.userData.first;
}
```

#### Error-handling
If you get a 500 INTERNAL SERVER ERROR, you probably just need to update your dependencies.  
1. From either your local or production shell prompt, source the virtual environment and navigate to the project root.  
2. Then type: ```python run.py```  
3. Read the error and install whatever missing dependencies you see via pip install DEPENDENCY.  
4. **Rinse and repeat til you get no errors**

### Authentication

By hooking into the identity services of various third parties like Google, Base avoids some of the complexities of managing identity (such as standing up an email service to verify email addresses).  This approach also allows your user to connect your application with the data they store with those third parties.  

With great power comes great *complexity*.  You'll need to store your client id and client secret in a safe place of both your local and production environments.  See the previous note on **Handling Secrets** for info on where to put this, but Base can access them if you name them like so:

```
GOOGLE_CLIENT_ID="your client id"
GOOGLE_SECRET="your secret"
```

This will allow your backend to access user data and keep it persistent for long periods of time.  You'll also need to replace my dummy client id in static/js/custom.js so that your front end is able to handle the initial login from the user.

##### setting up your google sign in client:
The examples below are all for Google Sign In, but you can easily tailor it to Facebook, Twitter, or any others.  So, to set up your Google Client ID, go to [Google Dev Console Credentials](https://console.developers.google.com/apis/credentials) and add a new project and OAuth Credentials.  Your credentials page should look something like this (substituting your domain for sourcewolf, of course):  

![Google Sign In](https://base.sourcewolf.com/static/img/google_sign_in.png)  

Copy your Client ID as you'll need to drop it into the **.env** file.

**External Authentication** Base is also intended to be used as an API, so you might wish to handle authentication elsewhere.  By copying over the auth head and foot to your remote application, you ensure that the same safe tokens are used to transact data between the two services, and you remote app will have full access to the data stored in the base CRM.  Note that authentication will fail on local unless you also create a file in the parent to the root folder of your project called redirect.html (already included in this package).  This file only needs to have a script tag that points to any file with hello.js code, so if that code is in your global header and footer already, this can just be an empty page.

#### Local https
If you require [https development over Flask](https://woile.github.io/posts/local-https-development-in-python-with-mkcert/) (to locally test facebook authentication, for example), you can just *brew install nss* and *brew install mkcert*.  Once those dependencies are installed on your local machine:
1. From the app root directory **cd /path/appname/** type: ```mkcert -cert-file cert.pem -key-file key.pem 0.0.0.0 localhost 127.0.0.1 ::1```
2. Comment out the app.run() at the bottom of **run.py** and comment in the next line:
```
  app.run(ssl_context=('cert.pem', 'key.pem'))
```
3. Now, when you type *python run.py* from app root, you should see **Running on https://127.0.0.1:5000/**
4. In your browser, navigate to **https://localhost:5000/**
