// 1. READ: https://adodson.com/hello.js/#quick-start
// 2. VISIT: https://console.developers.google.com/apis/credentials 
// 3. CLICK: Create Credentials > OAuth client ID (or access one you've already set up)
// 4. ADD: Your domain + client ID and Secret to your .env file

const absPath = ""; // leave blank if running locally. For remote, use like "https://yourdomain.com"
const authEndpoint = "/api/auth/"; // I: Google temp token O: persistent device token
const profileEndpoint = "/api/profile/"; // I: persistent device_token O: basic profile dictionary
const menuId = "userMenu"; // Refers to innerHTML bucket in components/nav.html

const loggedOutState = `<a class="menu-link" href="" onclick="login(); return false;">
                            <span class='authMeatball'><i class="fas fa-user-circle"></i></span> sign in
                        </a>`;

function displayLoggedIn(imageUrl) {
    loginState = `<a href="" class="menu-link" onclick="logout(); return false;">
                        <span class='authMeatball'>
                            <img style='width: 30px; border-radius: 50px; border: 1px solid;' src='`
                            + imageUrl +`' />
                        </span> sign out
                  </a>`;
    for ( const auth_status_bucket of document.getElementsByClassName(menuId) ) {
        auth_status_bucket.innerHTML = loginState;
    }
    document.getElementById(menuId).innerHTML = loginState;
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function deleteCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  console.log('Deleted cookie ' + name);
}

function logout() {
    var http = new XMLHttpRequest()
    var endpoint = absPath + authEndpoint;
    var data = {"token": getCookie('device_token')};
    http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            r = JSON.parse(this.response)
            if (r.error) {
                console.log('logout error: ' + r.error);
            } else {
                console.log('User made valid logout request and got: ' + JSON.stringify(r));
                document.cookie = deleteCookie("device_token");
                document.cookie = deleteCookie("profile");
                //document.getElementById(menuId).innerHTML = loggedOutState;
                for ( const auth_status_bucket of document.getElementsByClassName(menuId) ) {
                    auth_status_bucket.innerHTML = loggedOutState;
                }
                if (typeof clearNotes === "function") {
                    clearNotes();
                }
            }
        }
    }
    http.open('POST', endpoint, true);
    http.setRequestHeader('Content-Type', 'application/json');
    http.send(JSON.stringify(data));
}

function fetchProfile() {
    if ( getCookie("profile") ) {
        window.profile = JSON.parse(getCookie('profile'));
        return window.profile;
    } else {
        // no profile info, so must be start of a session.  fetch profile from remote.
        var http = new XMLHttpRequest()
        var endpoint = absPath + profileEndpoint;
        var data = fetchToken();
        http.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //console.log(this.response);
                var p = JSON.parse(this.response) // DON'T STORE THIS IN A COOKIE AS IT WILL OVERFLOW THE COOKIE
                if (p.error) {
                    // console.log('profile error: ' + p.error);
                    return p
                } else {
                    window.profile = p; // STORE EVERYTHING IN THE PAGE'S STORAGE
                    console.log('window.profile= ' + JSON.stringify(window.profile));
                    var basicProfile = {"email": p.email, "first": p.first, "last": p.last, "picture": p.picture};
                    document.cookie = 'profile=' + JSON.stringify(basicProfile) + ';' // max cookie size = 4096 bytes/characters.
                    console.log( "getCookie('profile')= " + getCookie('profile') );
                    displayLoggedIn(window.profile.picture);
                    return p;
                }
            }
        }
        http.open('POST', endpoint, true);
        http.setRequestHeader('Content-Type', 'application/json');
        http.send(JSON.stringify(data));
    }
}

function get_device_token(access_token) {
    var http = new XMLHttpRequest()
    var endpoint = absPath + authEndpoint;
    var data = {"access_token": access_token};
    http.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            r = JSON.parse(this.response)
            if (r.error) {
                console.log('login error: ' + r.error);
            } else {
                console.log('User made valid login request and got: ' + JSON.stringify(r));
                var userName = r.user.split('@')[0];
                document.cookie = "device_token=" + 
                    encodeURIComponent(r.token) + 
                    "; max-age=" + 10*365*24*60*60;
                window.device_token = r.token;
            }
            fetchProfile();
        }
    }
    http.open('POST', endpoint, true);
    http.setRequestHeader('Content-Type', 'application/json');
    http.send(JSON.stringify(data));
}

function login() {
    // initialize hello js to allow user to log in.
    hello.init({
        google: googleClientId
    }, {redirect_uri: '/redirect.html'});
    hello.on('auth.login', function (auth) {
        // get access token from response:
        var r = hello(auth.network).getAuthResponse();
        var access_token = r.access_token;
        var refresh_token = r.refresh_token;
        console.log('access_token: ' + access_token);
        get_device_token(access_token); // custom function to fetch token from api & set as cookie.
    });
    hello('google').login({scope: 'email'})
}

function fetchToken(){
    if (typeof window.device_token !== 'undefined') {
        device_token = window.device_token;
    } else {
        device_token = getCookie('device_token');
    }
    var data = {"token": device_token};
    return data;
}

// Profile functions that are not required for authentication are below this line.

postJSON = function(url, data){
    return new Promise(function(resolve,reject)
    {
        var req = new XMLHttpRequest();
        req.open('POST', url, true);
        req.setRequestHeader('Content-Type', 'application/json');
        req.onreadystatechange = function () {
        if (req.readyState == 4) {
            if(req.status == 200)
                resolve(JSON.parse(req.responseText));
            else
                reject(Error(req.statusText));
            }
        };
        req.onerror = function() {
            reject(Error("network error"));
        };
        req.send(JSON.stringify(data)); // or JSON.stringify(data) ?
    });
};

/*  Any time you want to add data for the logged in user, just use: 
        pushData(key, value) 
*/
async function pushData(key, value) {
    var data = fetchToken(); 
    if (data.token === "") { // unauthenticated users will be forced to login before editing data.
        login();
    }
    if (typeof window.userData !== "undefined") { // prevents overwriting.
        window.userData[key] = value;
        var url = absPath + profileEndpoint;
        data.meta = window.userData;
        console.log('Sent to ' + url + ' data:')
        console.log(data);
        var result = await postJSON(url, data);
    } else {
        console.log('Awaiting pullData');
    }
}

/*  This is a generic function that you'll need to repurpose on every page, depending on 
    where you want the data to go after it is initially loaded. For post-load events,
    though, you can simply call:
        window.userData[key] 
*/
async function pullData() {
    var url = absPath + profileEndpoint;
    var data = fetchToken(); 
    if (typeof window.userData === "undefined") { // ensure data wasn't already loaded.
        window.userData = await postJSON(url, data);
    }
    console.log('window.loaded = ' + window.loaded + ' and window.userData:');
    console.log(window.userData);
}