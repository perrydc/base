if (getCookie("device_token")) {
    // PAGE LOAD FOR LOGGED ON
    console.log('Page loaded for user with device_token: ' + getCookie('device_token'));
    var p = fetchProfile();
    if ( typeof(p) !== 'undefined' ) {
        console.log('Page loaded for profile: ' + JSON.stringify(p));
        window.fullProfile = p;
        displayLoggedIn(window.profile.picture);
    } else {
        console.log('device token but no profile');
    }
} else {
    console.log('device_token: ' + getCookie("device_token"));
    console.log('page loaded for user without a device_token');
    for ( const auth_status_bucket of document.getElementsByClassName(menuId) ) {
        auth_status_bucket.innerHTML = loggedOutState;
    }
}

// Connects form elements to a user's profile.  It is not required for authentication.
async function updateFormElements(){
    for ( const formElement of document.getElementsByClassName("userFormElement") ) {
        key = formElement.id;
        if (typeof window.userData === "undefined") { // ensure data wasn't already loaded.
            window.userData = await postJSON(absPath + profileEndpoint, fetchToken());
        }
        if ( key in window.userData ) {
            formElement.value = window.userData[key]
        }
        formElement.addEventListener(
            'change',
            function() { pushData(key, formElement.value); },
            false
        );
    }
}
updateFormElements();