# useful core functions used by run.py
from flask import Flask, jsonify, render_template, redirect, url_for, request, make_response, Markup
from flask_cors import CORS # allows cross-domain requests
import datetime, sys, pathlib, traceback, os 
from flask_misaka import Misaka # A handy Markdown translator

# helpers for telling your server how to route requests within the app
directory = pathlib.Path(__file__).parent
sys.path.insert(1, str(directory))
app = Flask(__name__)
cors = CORS(app) # make your app cross-domain accessible
Misaka(app, fenced_code=True) # allow markdown so your README.md documentation is readable
application = app #wsgi doesn't like abbreviations

# your local, custom modules
import model, model.users

# your custom template functions
@app.context_processor
# Model and view are distinct in Flask, but you may want to pass python functions to your templates.
# This example (used in templates/components/footer.html) can format a datetime object.
# {{ strftime(time, '%Y') }}
def strftime():
    def _strftime(time, form):
        return time.strftime(form)
    return dict(strftime=_strftime, now=model.now())

@app.context_processor
def inject_nav_vars():
    #return dict(routes=app.url_map.iter_rules()) # this would automate it from endpoints
    menu = [ # define the endpoints you want in the menu here
        'Profile', 
        'example_json_endpoint'
    ] 
    nav = []
    for item in menu:
        nav.append({"endpoint":item})
    return dict(routes=nav)

@app.context_processor
def inject_env_vars():
    # This is only for environment variables that you're OK exposing in your Javascript!
    environmentVariables = {
        "GOOGLE_CLIENT_ID": os.environ.get('GOOGLE_CLIENT_ID')
    }
    return dict(env=environmentVariables)
    
# Your Routes...
@app.route('/')
# http://127.0.0.1:5000 or https://yoursite.com
def index():
    documentation = open("README.md", "r")
    return render_template('index.html', documentation=documentation.read())
    
@app.route('/api', methods=['GET', 'POST'])
# an api that returns the timestamp plus any post or get data you send in the request.
# For a more direct connection between the endpoint and a database, see:
#   https://flask-restful.readthedocs.io/en/latest/quickstart.html#a-minimal-api
# or, if you're looking for something that can serve distributed clients at scale, see:
#   https://serverless.com/blog/flask-python-rest-api-serverless-lambda-dynamodb/
def example_json_endpoint():
    postData = {}
    if request.method == 'POST':
        postData = request.json
    # pass entire postData var to function, as below.
    getData = request.args
    # alternatively, pass args to different functions like getData['varName']
    return jsonify(model.exampleFunction(postData, getData))

@app.route('/redirect.html')
def redirect():
    # this is solely to complete the auth handshake (not a user-facing page)
    return render_template('base.html')

@app.route('/profile.html')
def Profile():
    # this example shows how you can store and retrieve form elements without additional js.
    return render_template('userProfile.html')

@app.route('/api/auth/', methods=['POST', 'GET'])
# exchanges a google access token for a permanent token OR logs the user out
def auth():
    postData = request.get_json()
    if postData:
        if 'access_token' in postData:
            return jsonify(model.users.login(postData['access_token']))
        else:
            return jsonify(model.users.logout(postData['token']))
    else:
        return jsonify(error = 'no post data')
    
@app.route('/api/profile/', methods=['POST', 'GET'])
# exchanges token for user profile information OR updates user profile
def profile():
    postData = request.get_json()
    if postData and 'token' in postData:
        if 'meta' in postData:
            return jsonify(model.users.writeProfile(postData['meta'], postData['token']))
        else:
            return jsonify(model.users.readProfile(postData['token']))
    else:
        return jsonify(error = 'no token in post data')
    
# error-handling
@app.errorhandler(Exception)
# this fallback route drastically simplifies debugging in Flask by showing you where your code broke.
def exception_handler(error):
  tb = traceback.format_exc().split('\n')
  return jsonify(error=repr(error), line=tb[-3], location=tb[-4], traceback=tb)

# https://woile.github.io/posts/local-https-development-in-python-with-mkcert/
# for local development (the if __name__ part), get the local ssl key.  Ignored on live.
if __name__ == "__main__":
    app.run() # run locally as http
    #app.run(ssl_context=('cert.pem', 'key.pem')) # run locally as https
