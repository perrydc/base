from datetime import datetime

def now():
    now = datetime.now()
    return now

def exampleFunction(postData, getData):
    return {
        'timestamp': now().isoformat(),
        'postData': postData,
        'getData': getData
    }