import requests, json, secrets, os, datetime
from peewee import *

def google(access_token):
    url = 'https://oauth2.googleapis.com/tokeninfo?access_token='+access_token
    resp = requests.get(url).json()
    if 'error' in resp:
        return {'error': 'this is either an invalid or expired token.'}
    if os.environ.get("GOOGLE_CLIENT_ID") != resp['azp']:
        return {'error': 'the access_token was not issued by Client ID in model/users/__init__.py.'}
    if resp['email_verified'] != 'true':
        return {'error': 'this email is not verified.'}
    return {
        'email': resp['email'],
    }

def login(access_token):
    # looks up a user's email and stores it with a permanent token, which it returns
    refresh_token = "" # not going to mess with this now
    google_resp = google(access_token)
    if 'error' in google_resp:
        return google_resp
    data = {
        "user": google_resp['email'],
        "access_token": access_token,
        "refresh_token": refresh_token,
        "stored": datetime.datetime.now(),
        "token": secrets.token_urlsafe(16)
    }
    Tokens.insert(**data).on_conflict('replace').execute()
    query = Profiles.select().where(Profiles.user == data['user'])
    if not query.exists():
        infoURL = 'https://www.googleapis.com/oauth2/v3/userinfo?access_token='+access_token
        userInfo = requests.get(infoURL).json()       
        profile = json.dumps({
            "email": data['user'],
            "first": userInfo['given_name'],
            "last": userInfo['family_name'],
            "picture": userInfo['picture']
        })
        newUser = {
            "user": data['user'],
            "profile": profile
        }
        Profiles.insert(**newUser).on_conflict('replace').execute()
    return data

def logout(token):
    Tokens.delete().where((Tokens.token == token)).execute()
    return {"status":"logged out"}

def readProfile(token):
    query = Profiles.select().join(Tokens, on=(Profiles.user==Tokens.user)).where(Tokens.token == token)
    if not query.exists():
        return {'error': 'user does not exist'}
    else: 
        user = query.dicts().get()
        return json.loads(user['profile'])

def writeProfile(profile, token):
    query = Profiles.select().join(Tokens, on=(Profiles.user==Tokens.user)).where(Tokens.token == token)
    if not query.exists():
        return {'error': 'user does not exist'}
    else: 
        data = query.dicts().get()
        userUpdate = {
            "user": data['user'],
            "profile": json.dumps(profile)
        }
        Profiles.insert(**userUpdate).on_conflict('replace').execute()
        return json.loads(userUpdate['profile'])
    
dbPath = 'model/users/data.db'
db = SqliteDatabase(dbPath)

class BaseModel(Model):
    class Meta:
        database = db
    
class Profiles(BaseModel):
    profile = TextField(default='') # json.dumps(profile Dictionary)
    user = CharField(unique=True) # name@email.com
    
class Tokens(BaseModel):
    token = CharField(index=True) # kjok-93e8djui stored in device cookie
    access_token = CharField() # access_token from 3rd party, like google
    refresh_token = CharField() # refresh_token from 3rd party, like google
    stored = DateTimeField() # datetime.datetime.now()
    user = ForeignKeyField(Profiles, backref='tokens') # name@email.com
    
if not os.path.exists(dbPath):
    db.connect()
    db.create_tables([Tokens, Profiles])